import Vue from "vue";
import VueRouter from "vue-router";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-default/index.css';
import 'font-awesome/css/font-awesome.css';
import VueResource from 'vue-resource';
import Mint from 'mint-ui';
import 'mint-ui/lib/style.css';
// import echarts from 'echarts';
//pc 
import PApp from './components/pc/App';
import PRegister from './components/pc/Register';
import PRegisterProduct from './components/pc/RegisterProduct';
import PStar from './components/pc/Star';
import PLogin from './components/pc/Login';
import POnMsgList from './components/pc/OnMsgList';
import POffMsgList from './components/pc/OffMsgList';
import PNewUser from './components/pc/newUser';
import PChangePass from './components/pc/changePass';
import PProductList from './components/pc/productList';
import PInfluenserList from './components/pc/influenserList';
import PSourceData from './components/pc/SourceData';
import DashBord from './components/pc/dash/DashBord';
import CheckProduct from './components/pc/dash/CheckProduct';

import Upcoming from './components/pc/dash/Upcoming';
import Company from './components/pc/company/Company';
import CompanyDetails from './components/pc/company/CompanyDetails';
import AddNewCompany from './components/pc/company/AddNewCompany';
import ProductDetails from './components/pc/company/ProductDetails';
import AddProduct from './components/pc/company/AddProduct';
import InfluencerList from './components/pc/influencer/InfluenserList';
import AddInfluencer from './components/pc/influencer/AddInfluencer';
import InfluencerDetails from './components/pc/influencer/InfluencerDetails';

import Money from './components/pc/money/Money';
import SystemManage from './components/pc/system/SystemManage';
//m
import MApp from './components/m/App';
import MMenu from './components/m/Menu';
import MRegisterInfluenser from './components/m/RegisterInfluenser';
import MRegisterProduct from './components/m/RegisterProduct';
import MInfluenserList from './components/m/InfluenserList';
import MProductList from './components/m/ProductList';
import MMsgData from './components/m/MsgData';
import MMsgDetails from './components/m/MsgDetail';

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Mint);
Vue.use(ElementUI);
// Vue.use(echarts);
const userAgent = (window.navigator && navigator.userAgent) || "";
let isMobile = (/(iphone|ipod|((?:android)?.*?mobile)|blackberry|nokia)/i).test(userAgent);;
// console.log('----------', userAgent, isMobile);
let routes = [];
if (isMobile) {

    // mobile router
    routes = [{
            path: '/mapp',
            name: 'mapp',
            component: MApp,
            children: [
                { path: 'mmenu', component: MMenu },
                { path: 'mregisterInfluenser', component: MRegisterInfluenser },
                { path: 'mregisterProduct', component: MRegisterProduct },
                { path: 'minfluenserList', component: MInfluenserList },
                { path: 'mproductList', component: MProductList },
                { path: 'mmsgData', component: MMsgData },
                { path: 'mmsgDetails/:id/:date/:type/:os', component: MMsgDetails }
            ]
        },
        { path: '/', redirect: '/mapp/mmenu' },
        { path: '*', component: MApp }
    ];
} else {

    // pc router
    routes = [{
            path: '/app',
            name: 'app',
            component: PApp,
            children: [
                { path: 'onmsgList', component: POnMsgList },
                { path: 'offMsgList', component: POffMsgList },
                { path: 'sourceData', component: PSourceData },
                { path: 'register', component: PRegister },
                { path: 'registerProduct', component: PRegisterProduct },
                { path: 'star', component: PStar },
                { path: 'productList', component: PProductList },
                { path: 'influenserList', component: PInfluenserList },
                { path: 'dashBord', component: DashBord },
                { path: 'checkProduct', component: CheckProduct },
                { path: 'upcoming', component: Upcoming },
                { path: 'company', component: Company },
                { path: 'companyDetails/:id', component: CompanyDetails },
                { path: 'addNewCompany', component: AddNewCompany },
                { path: 'productDetails/:id', component: ProductDetails },
                { path: 'addProduct', component: AddProduct },
                { path: 'influencerList', component: InfluencerList },
                { path: 'addInfluencer', component: AddInfluencer },
                { path: 'influencerDetails', component: InfluencerDetails },
                { path: 'money', component: Money },
                { path: 'systemManage', component: SystemManage },
            ]
        },
        { path: '/login', component: PLogin },
        { path: '/newUser', component: PNewUser },
        { path: '/changePass', component: PChangePass },
        { path: '/', redirect: '/app/star' },
        { path: '*', component: PApp }
    ];
}
const router = new VueRouter({
    // mode: 'history',
    hashbang: false, //将路径格式化为#!开头
    history: true, //启用HTML5 history模式
    abstract: false, //使用一个不依赖于浏览器的浏览历史虚拟管理后端
    transitionOnLoad: false, //初次加载是否启用场景切换
    saveScrollPosition: false, //在启用html5 history模式的时候生效，用于后退操作的时候记住之前的滚动条位置
    linkActiveClass: "v-link-active", //链接被点击时候需要添加到v-link元素上的class类,默认为active
    routes: routes
});

new Vue({
    el: '#app',
    router
});

// new Vue({
//   router
// }).$mount('#app')