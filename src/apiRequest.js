import axios from "axios";
import store from 'store2';
import qs from 'qs';

function baseRequest(method, path, params, data, type) {
    method = method.toUpperCase() || 'GET';
    let baseul = '';
    let paramsobj = { params: params };
    // 真是环境
    // if (type === 'msg') {
    //     baseul = 'http://52.41.103.232:81';
    // } else {
    //     baseul = 'http://52.11.159.45:3000';
    // }
    // 测试环境
    if (type === 'msg') {
        baseul = 'http://52.41.181.214:81';
    } else {
        baseul = 'http://52.42.99.98:3000';
    }
    axios.defaults.baseURL = baseul;
    if (method === 'POST') {
        axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        return axios.post(path, qs.stringify(data));
    } else if (method === 'GET') {
        return axios.get(path, paramsobj);
    } else {
        return axios.delete(path, qs.stringify(data));
    };
}

exports.registerInfluencer = function registerInfluencer(data) {
    return baseRequest('POST', '/influencer', '', data);
};

exports.registerInfluencerLink = function registerInfluencerLink(data) {
    return baseRequest('POST', '/influencer_link_product', '', data);
};

exports.registerProduct = function registerProducts(data) {
    return baseRequest('POST', '/product', '', data);
};

exports.getProductList = function productList(params) {
    return baseRequest('GET', '/product', params, '');
};

exports.getMsgListRequest = function msgListRequest(params) {
    return baseRequest('GET', '/statistics', params, '', 'msg');
};

exports.getInfluencerPid = function influencerPid(pid) {
    return baseRequest('GET', '/influencer_product_assoc/' + pid, '', '', '');
};

exports.getInfluencer = function influencer() {
    return baseRequest('GET', '/influencer', '', '', '');
};
// updata influenser
exports.updateInfluencer = function updateInfluencer(data) {
    return baseRequest('POST', '/update_influencer', '', data);
};
exports.updateInfluencerLink = function updateInfluencerLink(data) {
    return baseRequest('POST', '/update_influencer_link_product', '', data);
};
// remove influenser
exports.removeInfluenser = function removeInfluenser(id) {
    return baseRequest('GET', '/del_influencer/' + id, '', '');
};
// exports.removeInfluenserLink = function removeInfluenserLink(id) {
//     return baseRequest('GET', '/del_influencer_link_product/'+id, '', '');
// };
// updata product
exports.updateProduct = function updateProduct(data) {
    return baseRequest('POST', '/update_product', '', data);
};
// remove product
exports.removeProduct = function removeProduct(id) {
    return baseRequest('GET', '/del_product/' + id, '', '');
};

// ck 去重
exports.getCkHourly = function ckHourly(params) {
    return baseRequest('GET', '/ck_hourly', params, '');
};
// pb 去重
exports.getPbHourly = function pbHourly(params) {
    return baseRequest('GET', '/pb_hourly', params, '');
};
// 来源
exports.getCkFromDaily = function ckFromDaily(params) {
    return baseRequest('GET', '/ck_from_daily', params, '');
};
// name 获取关联 id
exports.getContactId = function contactId(params) {
    return baseRequest('GET', '/get_influ_product_assoc', params, '');
};